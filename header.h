#pragma once
#ifndef HEADER_H
#define HEADER_H


typedef struct prijava {
	char ime[20];
	char prezime[30];
	char adresa[70];
} PRIJAVA;




typedef struct fotoaparatiiobjektivi {
	int index;
	int kolicina;


}FOTO;


typedef struct servis {
	char uredaj[40];
	char kvar[300];
}SERVIS;

typedef struct usluge {
	char datum[30];
	char kontakt[20];
}USLUGE;

int odabirNaIzborniku();
int funkcijaTrgovina(PRIJAVA*, FILE*, FILE*);
int funkcijaServis(PRIJAVA*, FILE*);
int funkcijaUsluge(PRIJAVA*, FILE*);
int odabirFotoaparata(PRIJAVA*, FILE*,FILE*);
void unosPodataka1(PRIJAVA*, int, FILE*,FILE*);
int odabirObjektiva(PRIJAVA*, FILE*,FILE*);
void unosPodataka2(PRIJAVA*, int, FILE*,FILE*);
void citanjeDatoteke(FILE* fp2);
#endif
