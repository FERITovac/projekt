#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "header.h"


int main()
{

	int n = 0;
	FILE* uslugep = NULL;
	uslugep = fopen("usluge.txt", "a+");
	if (uslugep == NULL)
		perror("otvaranje");
	FILE* servisp = NULL;
	servisp = fopen("servis.txt", "a+");
	if (servisp == NULL)
		perror("otvaranje");
	FILE* fp = NULL;
	FILE* fp2 = NULL;
	fp2 = fopen("trgovina.bin", "ab+");
	fp = fopen("trgovina.txt", "a+");
	if (fp == NULL)
		perror("otvaranje");
	if (fp2 == NULL)
		perror("otvaranje");
	PRIJAVA* prijava;
	prijava = (PRIJAVA*)calloc(3, sizeof(PRIJAVA));
	printf("=============================== Canora ===============================\n");
	printf("=  PRIJAVA                                                           =\n");
	printf("======================================================================\n");
	printf("=  Unesi ime:                                                        =\n");
	printf("======================================================================\n");

	scanf("%69[^\n]", prijava->ime);
	getchar();
	system("cls");
	printf("=============================== Canora ===============================\n");
	printf("=  PRIJAVA                                                           =\n");
	printf("======================================================================\n");
	printf("=  Unesi prezime:                                                    =\n");
	printf("======================================================================\n");

	scanf("%69[^\n]", prijava->prezime);
	getchar();
	system("cls");
	printf("=============================== Canora ===============================\n");
	printf("=  PRIJAVA                                                           =\n");
	printf("======================================================================\n");
	printf("=  Unesi adresu:                                                     =\n");
	printf("======================================================================\n");

	scanf("%69[^\n]", prijava->adresa);
	getchar();
	system("cls");
	pocetak:
	n = odabirNaIzborniku();
	switch (n) {
	case 1: funkcijaTrgovina(prijava,fp,fp2); break;
	case 2: funkcijaServis(prijava,servisp); break;
	case 3: funkcijaUsluge(prijava,uslugep); break;
	case 0: return 0;
	}

	printf("=============================== Canora ===============================\n");
	printf("=  Jeste li gotovi:                                                  =\n");
	printf("======================================================================\n");
	printf("=  [1]  Da                                                           =\n");
	printf("=  [0]  Ne                                                           =\n");
	printf("======================================================================\n");
	do {
		scanf("%d", &n);
		if (n < 0 || n > 1)
			printf("Pogresan unos.\nPokusajte ponovo.\n");
	} while (n < 0 || n > 1);
	fclose(uslugep);
	fclose(servisp);
	fclose(prijava);
	fclose(fp);
	fclose(fp2);
	switch (n)
	{
	case 1: return 1; break;
	case 0: goto pocetak; break;
	}

	return 0;
}
