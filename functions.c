#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "header.h"


int odabirNaIzborniku()
{
	int n;
	system("cls");
	printf("=============================== Canora ===============================\n");
	printf("=  Odaberite jednu od ponudjenih opcija:                             =\n");
	printf("======================================================================\n");
	printf("=  [1]  Trgovina                                                     =\n");
	printf("=  [2]  Servis                                                       =\n");
	printf("=  [3]  Foto usluge                                                  =\n");
	printf("=  [0]  Izlaz                                                        =\n");
	printf("======================================================================\n");

	do {
		scanf("%d", &n);
		if (n < 0 || n > 3)
			printf("Pogresan unos.\nPokusajte ponovo.\n");
	} while (n < 0 || n > 3);

	return n;
	
}
int funkcijaUsluge(PRIJAVA* prijava, FILE* uslugep)
{
	
	USLUGE usl;
	system("cls");
	printf("=============================== Canora ===============================\n");
	printf("Kontaktirat ćemo vas u vezi odabira i cijene ili u slučaju da je datum zauzet\n");
	printf("Unesite zeljeni datum\n");
	scanf("%30s", usl.datum);
	printf("\nKontakt broj: ");
	scanf("%20s", usl.kontakt);

	fprintf(uslugep, " %s \n    -> Ime: %s\n    -> Prezime: %s\n    -> Kontakt: %s", usl.datum,prijava->ime,prijava->prezime,usl.kontakt);

}


int funkcijaServis(PRIJAVA*prijava,FILE* servisp)
{
	SERVIS servis;
	system("cls");
	printf("=============================== Canora ===============================\n");
	printf("=  Unesite sve podatke koje vass trazimo:                            =\n");
	printf("======================================================================\n");
	printf("\nUnesite uredaj koji treba servisirati:");
	getchar();
	scanf("%39[^\n]", servis.uredaj);
	getchar();
	printf("\nUkratko opisite kvar:");
	scanf("%299[^\n]", servis.kvar);
	getchar();
	fprintf(servisp, "== %s == \n-> Ime vlasnika: %s\n-> Prezime vlasnika: %s\n-> Adresa vlasnika: %s\n-> Opis kvara: %s\n\n",servis.uredaj,prijava->ime,prijava->prezime,prijava->adresa,servis.kvar);
	return 0;
}

int funkcijaTrgovina(PRIJAVA*prijava,FILE* fp, FILE* fp2)
{
	int n,odabirF;

	system("cls");
	printf("=============================== Canora ===============================\n");
	printf("=  Odaberite jednu od ponudjenih opcija:                             =\n");
	printf("======================================================================\n");
	printf("=  [1]  Kupovina Fotoaparata                                         =\n");
	printf("=  [2]  Kupovina objektiva i opreme                                  =\n");
	printf("=  [0]  Izlaz                                                        =\n");
	printf("======================================================================\n");
	

	do {
		scanf("%d", &n);
		if (n < 0 || n > 2)
			printf("Pogresan unos.\nPokusajte ponovo.\n");
	} while (n < 0 || n > 2);
	int status = 1;
	switch (n) {
	case 1: {
		system("cls");
		status = odabirFotoaparata(prijava, fp,fp2);
		
		break;
	}
	{
	case 2:
		system("cls");
		status = odabirObjektiva(prijava, fp,fp2);
		break;
	}
	case 0: {
		return 0;
		break;
	}
	}
	return;
}

int odabirFotoaparata(PRIJAVA* prijava, FILE* fp,FILE* fp2)
{
		system("cls");
		printf("================ Canora ================\n");
		printf("= Odaberite fotoaparata.               =\n");
		printf("========================================\n");
		printf("= [1] Canon EOS 5D Mark IV             =\n");
		printf("= [2] Canon EOS 6D Mark III            =\n");
		printf("= [3] Canon EOS RP                     =\n");
		printf("= [4] Canon EOS R                      =\n");
		printf("= [5] Canon EOS R6                     =\n");
		printf("= [6] Canon EOS R5                     =\n");
		printf("========================================\n");
		printf("= [7] Prednaruči Canon EOS R3          =\n");
		printf("========================================\n");
		printf("= [0] Izlaz                            =\n");
		printf("========================================\n");

		int n;
		int odabirF;
		do {
			scanf("%d", &n);
			if (n < 0 || n > 8)
				printf("Pogresan unos.\nPokusajte ponovo.\n");
		} while (n < 0 || n > 8);
		int i;
		switch (n){
		case 1: {
			unosPodataka1( prijava, n, fp, fp2);
			return 0;
			break;
		}
		{
		case 2:
			unosPodataka1(prijava, n, fp, fp2);
			return 0;
			break;
		}
		case 3: {
			unosPodataka1(prijava, n, fp, fp2);
			return 0;
			break;
		}
		case 4: {
			unosPodataka1(prijava, n, fp, fp2);
			return 0;
			break;
		}
		case 5: {
			unosPodataka1(prijava, n, fp,fp2);
			return 0;
			break;
		}
		case 6: {
			unosPodataka1(prijava, n, fp,fp2);
			return 0;
			break;
		}
		case 7: {
			unosPodataka1( prijava, n, fp,fp2);
			return 0;
			break;
		}
		case 8: {
			unosPodataka1( prijava, n, fp, fp2);
			return 0;
			break;
		}
		case 0: {
			return 0;
			break;
		}
	}
}

void unosPodataka1(PRIJAVA* prijava, int n, FILE* fp, FILE* fp2)
{
	FOTO foto;
	int i = 0;
	if (n == 1) {
		
		printf("\nOdabrali ste 5D Mark IV\n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "==  Canon EOS 5D Mark IV [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n",  foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(prijava), sizeof(prijava)/sizeof(prijava), fp2);
		fclose(fp);
	}
	if (n == 2) {
		
		printf("\nOdabrali ste 6D Mark III\n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		printf("\nUnesite ime narucitelja:  ");
		fprintf(fp, "==  Canon EOS 6D Mark III [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n",  foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(prijava), sizeof(prijava) / sizeof(prijava), fp2);
		fclose(fp);
	}
	if (n == 3) {
		
		printf("\nOdabrali ste RP \n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "==  Canon EOS RP [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n",  foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(prijava), sizeof(prijava) / sizeof(prijava), fp2);
		fclose(fp);
	}
	if (n == 4) {
		
		printf("\nOdabrali ste R\n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "==  Canon EOS R [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n",  foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(prijava), sizeof(prijava) / sizeof(prijava), fp2);
		fclose(fp);
	}
	if (n == 5) {
		
		printf("\nOdabrali ste R6\n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "==  Canon EOS R6 [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n",  foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(prijava), sizeof(prijava) / sizeof(prijava), fp2);
		fclose(fp);
	}
	if (n == 6) {
		
		printf("\nOdabrali ste R5\n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "==  Canon EOS R5 [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n",  foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(prijava), sizeof(prijava) / sizeof(prijava), fp2);
		fclose(fp);
	}
	if (n == 7) {
		
		printf("\nOdabrali ste R3\n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naruciti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "==  Canon EOS R3 [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n",  foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(prijava), sizeof(prijava) / sizeof(prijava), fp2);
		fclose(fp);
	}
	if(n==8) {
		int polje = calloc(sizeof(fp2) + 1, sizeof(char));
		citanjeDatoteke(fp2, polje);
	}
	i++;
}

int odabirObjektiva(PRIJAVA* prijava, FILE* fp, FILE* fp2)
{
	system("cls");
	printf("================ Canora ================\n");
	printf("= Odaberite objektiv.                  =\n");
	printf("========================================\n");
	printf("= [1] Canon 24-70mm f/2.8              =\n");
	printf("= [2] Canon 70-200mm f/4               =\n");
	printf("= [3] Canon 70-200mm f/2.8             =\n");
	printf("= [4] Canon 50mm f/1.2                 =\n");
	printf("= [5] Canon 85mm f/1.2                 =\n");
	printf("= [6] Canon 28-70mm f/2.8              =\n");
	printf("= [7] Fototripod                       =\n");
	printf("========================================\n");
	printf("= [0] Izlaz                            =\n");
	printf("========================================\n");

	int n;
	int odabirF;
	do {
		scanf("%d", &n);
		if (n < 0 || n > 7)
			printf("Pogresan unos.\nPokusajte ponovo.\n");
	} while (n < 0 || n > 7);
	int i;
	switch (n) {
	case 1: {
		unosPodataka2(prijava, n, fp, fp2);
		return 0;
		break;
	}
	{
	case 2:
		unosPodataka2(prijava, n, fp, fp2);
		return 0;
		break;
	}
	case 3: {
		unosPodataka2(prijava, n, fp, fp2);
		return 0;
		break;
	}
	case 4: {
		unosPodataka2(prijava, n, fp, fp2);
		return 0;
		break;
	}
	case 5: {
		unosPodataka2(prijava, n, fp, fp2);
		return 0;
		break;
	}
	case 6: {
		unosPodataka2(prijava, n, fp, fp2);
		return 0;
		break;
	}
	case 7: {
		unosPodataka2(prijava, n, fp, fp2);
		return 0;
		break;
	}
	case 0: {
		return 0;
		break;
	}
	}
}

void unosPodataka2(PRIJAVA* prijava, int n, FILE* fp, FILE* fp2)
{

	FOTO foto;
	int i = 0;
	if (n == 1) {
		printf("\nOdabrali ste \n[1] Canon 24-70mm f/2.8  \n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "%d==  Canon 24-70mm f/2.8 [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n",i+1,  foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(FOTO), sizeof(FOTO) / sizeof(prijava), fp2); 
		fclose(fp);
	}
	if (n == 2) {

		printf("\nOdabrali ste \n[2] Canon 70-200mm f/4 \n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "%d==  Canon 70-200mm f/4 [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n",i+1,  foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(FOTO), sizeof(FOTO) / sizeof(prijava), fp2); 
		fclose(fp);
	}
	if (n == 3) {

		printf("\nOdabrali ste  \n[3] Canon 70-200mm f/2.8 \n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "%d==  Canon 70-200mm f/2.8 [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n",i,  foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(FOTO), sizeof(FOTO) / sizeof(prijava), fp2); 
		fclose(fp);
	}
	if (n == 4) {

		printf("\nOdabrali ste \n[4] Canon 50mm f/1.2   \n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "==  Canon 50mm f/1.2 [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n",  foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(FOTO), sizeof(FOTO) / sizeof(prijava), fp2); 
		fclose(fp);
	}
	if (n == 5) {

		printf("\nOdabrali ste \n[5] Canon 85mm f/1.2   \n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "==  Canon 85mm f/1.2 [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n", foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(FOTO), sizeof(FOTO) / sizeof(prijava), fp2); 
		fclose(fp);
	}
	if (n == 6) {

		printf("\nOdabrali ste\n[6] Canon 28-70mm f/2.8	\n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "==  Canon 28-70mm f/2.8 [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n", foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(FOTO), sizeof(FOTO) / sizeof(prijava), fp2); 
		fclose(fp);
	}
	if (n == 7) {

		printf("\nOdabrali ste\n[7] prijavatripod\n");
		foto.index = n;
		printf("\nUnesite kolicinu koju zelite naručiti:  ");
		scanf("%d", &foto.kolicina);
		fprintf(fp, "==  Fototripod [x%d] == \n-> Ime narucitelja: %s\n-> Prezime narucitelja: %s\n-> Adresa dostave: %s\n\n", foto.kolicina, prijava->ime, prijava->prezime, prijava->adresa);
		fwrite(&prijava, sizeof(FOTO), sizeof(FOTO) / sizeof(prijava), fp2); 
		fclose(fp);
	}
}
void citanjeDatoteke(FILE* fp2)
{
	int i=0;
	
	FOTO* foto = calloc(sizeof(fp2),sizeof(int));
	if (foto == NULL)
		return 0;
	while (i<=sizeof(fp2)) {
		printf("%d\n", i);
		i++;
	}
	free(foto);
}


